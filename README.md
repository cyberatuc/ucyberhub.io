# ucyber.github.io

This website is no longer used! The code is kept here for historical purposes.

Our new website's repository can be found at **[gitlab.com/cyberatuc/cyberatuc.org](https://gitlab.com/cyberatuc/cyberatuc.org)**.